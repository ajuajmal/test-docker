# sds-dynamic

A django project for sds


# Run : Local

* clone the project
* create  python enviornment using virtual env 
   `virtualenv env`  
* cd to project
* install requirements using pip `pip3 insatll -r requirements.txt`
* set env variables by `cp .env.sample .env`
* make migrations `python3 manage.py makemigrations`
* migarte `python3 manage.py migrate`
* run the dev server again `python3 manage.py runserver`


# Contribution Guide 
  
  
 [Studevsoc Doc](https://doc.studevsoc.com/)
